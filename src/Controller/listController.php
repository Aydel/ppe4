<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Entity\Produit;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Description of listController
 *
 * @author Aymerich
 */
class listController extends AbstractController {

    /**
     * @Route("/produits", name="produits")
     */
    public function listerProduits(EntityManagerInterface $em) {
        $produits = $em->getRepository(Produit::class)->findAll();


        return $this->render('produits/list.html.twig', array(
                    'produits' => $produits,
        ));
    }

    /**
     * @Route("/listProdCat/{id}",name="listProdCat")
     */
    public function listProdCat(EntityManagerInterface $em, $id) {
        $lesProdCat = $em->getRepository(Produit::class)->findBy(['categorie' => $id]);

        return $this->render('produits/list.html.twig', array(
                    'produits' => $lesProdCat,
        ));
    }

}
