<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Entity\Produit;
use App\Entity\PanierLigne;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Description of produitController
 *
 * @author Aymerich
 */
class produitController extends AbstractController {

    /**
     * @Route("/produit/{id}",name="dProduit")
     */
    public function showProduit(EntityManagerInterface $em, $id) {

        $prod = $em->getRepository(Produit::class)->find($id);

        return $this->render('produits/show.html.twig', array(
                    'prod' => $prod,
        ));
    }

    /**
     * 
     * @Route("/add/{id}",name="addPznier")
     */
    public function addPanier(EntityManagerInterface $em, $id) {

        $user = $this->get('security.token_storage')->getToken()->getUser();

        
        if($user == "anon."){
            return $this->redirectToRoute('login');
        }
        
        $panier = $em->getRepository(PanierLigne::class)->findBy(['utilisateur' => $user->getId(), 'produit' => $id]);

        if ($panier != null) {
            $ligne = $panier[0];
            $old = $ligne->getQuantite();
            $ligne->setQuantite($old+1);
        } else {
            $ligne = new PanierLigne();

            $prod = $em->getRepository(Produit::class)->find($id);

            $ligne->setProduit($prod);
            $ligne->setQuantite(1);
            $ligne->setUtilisateur($user);
        }
        $em->persist($ligne);
        $em->flush();

        return $this->redirectToRoute('panier');
    }

}
