<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Entity\Utilisateurs;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Description of loginController
 *
 * @author Aymerich
 */
class loginController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController {


    /**
     * @Route("/login",name="login")
     */
    public function login(AuthenticationUtils $auth) {
        $nbr = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        $mdpT = [];
        $n = 0;
        for ($j = 0; $j < 2; $j++) {
            for ($i = 0; $i < 5; $i++) {
                $min = reset($nbr);
                $rand = rand($min, count($nbr));
                if ($n < 10) {
                    while ((!isset($nbr[$rand]))) {
                        $rand = rand($min, count($nbr));
                    }
                    $nb = $nbr[$rand];
                    $mdpT[$j][$n] = $nb;
                    unset($nbr[$nb]);
                }
                $n++;
            }
        }
        $erreur = $auth->getLastAuthenticationError();
        $lastUserName = $auth->getLastUsername();
        return $this->render("login/loginAccueil.html.twig", array(
                    'last_username' => $lastUserName, 'error' => $erreur, 'table' => $mdpT));
    }

}
