<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Produit;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Utilisateurs;
use App\Entity\Categorie;
use App\Entity\PanierLigne;
use App\Entity\Adresse;
use App\Entity\Commande;
use App\Entity\Status;
use DateTime;
use App\Entity\CommandeLigne;

/**
 * Description of apiProduitController
 *
 * @author Aymerich
 */
class apiProduitController extends AbstractController {

    /**
     * @Route("/apiProduit/{id}", name="apiProd")
     */
    public function apiGetProduitMethodeClassique($id, EntityManagerInterface $em) {
        $unProduit = $em->getRepository(Produit::class)->findById($id);

        $unProduit[0]->setSelections(null);
        $unProduit[0]->setCategorie(null);
//        $normalizer = new ObjectNormalizer();
//        $normalizer->setCircularReferenceLimit(0);
//        $normalizer->setCircularReferenceHandler(function ($unProduit) {
//            return $unProduit->getId();
//        });
//        $normalizers = array($normalizer);
//        $encoders = array(new XmlEncoder(), new JsonEncoder());
//        $serializer = new Serializer($normalizers, $encoders);
        $serializer = $this->get('serializer');
        $data = $serializer->serialize($unProduit[0], 'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Ok', 'oui');
        return $response;
    }

    /**
     * @Route("/apiAllproduits",name="apiAllProd")
     */
    public function apiGetAllProduits(EntityManagerInterface $em) {
        $produits = $em->getRepository(Produit::class)->findAll();

        foreach ($produits as $prod) {
            $prod->setSelections(null);
        }
        $serializer = $this->get('serializer');
        $data = $serializer->serialize($produits, 'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Ok', 'oui');
        return $response;
    }

    /**
     * @Route("/apiProduitByC/{id}",name="apiProduitByC")
     */
    public function apiProduitByC($id, EntityManagerInterface $em) {

        $produits = $em->getRepository(Produit::class)->findBy([
            "categorie" => $id]);

        foreach ($produits as $unProduit) {
            $unProduit->setSelections(null);
            $unProduit->setCategorie(null);
        }

        $serializer = $this->get('serializer');
        $data = $serializer->serialize($produits, 'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Ok', 'oui');

        return $response;
    }

    /**
     * @Route("/apiAddProduit",name="apiAddProd")
     */
    public function apiAddProduit(Request $request, EntityManagerInterface $em) {
        //$serializer = $this->get('serializer');
        //$leProduit = $serializer->deserialize($request->getContent(), Produit::class, 'json');

        $leProduit = new Produit();
        $leProduit->setLibelle($request->get("_libelle"));
        $leProduit->setDescription($request->get("_description"));
        $leProduit->setImage($request->get("_image"));
        $leProduit->setPrix(intval($request->get("_prix")));
        $leProduit->setStock(intval($request->get("_stock")));
        $cat = $em->getRepository(Categorie::class)->find(intval($request->get("_categorie")));

        $leProduit->setCategorie($cat);
        $leProduit->setSelections(null);

        $em->persist($leProduit);
        $em->flush();

        $response = new Response("L'ajout est réalisé");
        $response->headers->set('Content-Type', 'application/text');
        $response->headers->set('Ok', 'oui');
        return $response;
    }

    /**
     * @Route("/apiCatProduit",name="apiCatProduit")
     */
    public function apiCatProduit(EntityManagerInterface $em) {
        $categories = $em->getRepository(Categorie::class)->findAll();

        foreach ($categories as $unecat) {
            $unecat->setLesProduits(null);
        }

        $serializer = $this->get('serializer');
        $data = $serializer->serialize($categories, 'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Ok', 'oui');

        return $response;
    }

    /**
     * @Route("/apiEditProd",name="apiEditProd")
     */
    public function apiEditProd(Request $request, EntityManagerInterface $em) {
        $leProd = $em->getRepository(Produit::class)->find($request->get('_id'));
        $leProd->setLibelle($request->get("_libelle"));
        $leProd->setDescription($request->get("_description"));
        $leProd->setPrix($request->get("_prix"));
        $leProd->setStock($request->get("_stock"));

        $em->persist($leProd);
        $em->flush();

        $response = new Response("Edité");
        $response->headers->set('Content-Type', 'application/text');
        $response->headers->set('Ok', 'oui');
        return $response;
    }

    /**
     * @Route("/apiAddPanierProd",name="apiAddPanierProd")
     */
    public function apiAddPanierProd(EntityManagerInterface $em, Request $request) {
        $user = $em->getRepository(Utilisateurs::class)->find($request->get("_idUser"));
        $produit = $em->getRepository(Produit::class)->find($request->get("_idProduit"));

        $panier = $em->getRepository(PanierLigne::class)->findBy(['utilisateur' => $user->getId(), 'produit' => $produit->getId()]);

        if ($panier != null) {
            $pnL = $panier[0];
            $old = $pnL->getQuantite();
            $pnL->setQuantite($old + 1);
        } else {
            $pnL = new PanierLigne();

            $pnL->setProduit($produit);
            $pnL->setUtilisateur($user);
            $pnL->setQuantite(1);
        }
        $em->persist($pnL);
        $em->flush();

        $response = new Response("add");
        $response->headers->set('Content-Type', 'application/text');
        $response->headers->set('Ok', 'oui');
        return $response;
    }

    /**
     * @Route("/apiGetPanier/{id}",name="apiGetPanier")
     */
    public function apiGetPanier(EntityManagerInterface $em, $id) {
        $listPanier = $em->getRepository(PanierLigne::class)->findBy(['utilisateur' => $id]);

        foreach ($listPanier as $uneL) {
            $uneL->getUtilisateur()->setAdresses(null);
            $uneL->getUtilisateur()->setCommandes(null);
            $uneL->getUtilisateur()->setProduitsPanier(null);
            $uneL->getProduit()->setSelections(null);
            $uneL->getProduit()->setCategorie(null);
        }

        $serializer = $this->get('serializer');
        $data = $serializer->serialize($listPanier, 'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Ok', 'oui');

        return $response;
    }

    /**
     * @Route("/apiOrderPanier",name="apiOrderPanier") 
     */
    public function apiOrderPanier(EntityManagerInterface $em, Request $request) {

        $user = $em->getRepository(Utilisateurs::class)->find($request->get("_userId"));
        $adressesUser = $em->getRepository(Adresse::class)->findBy(['utilisateur' => $request->get("_userId")]);
        $produitsUser = $em->getRepository(PanierLigne::class)->findBy(['utilisateur' => $request->get("_userId")]);

        $commande = new Commande();

        $commande->setUtilisateur($user);
        $commande->setAdresse($adressesUser[0]);

        $status = $em->getRepository(Status::class)->find(1);

        $commande->setStatus($status);
        $commande->setDate(new DateTime());

        $total = 0;

        foreach ($produitsUser as $ligne) {
            $commandeLigne = new CommandeLigne();
            $commandeLigne->setCommande($commande);
            $commandeLigne->setProduit($ligne->getProduit());
            $commandeLigne->setQuantite($ligne->getQuantite());
            $em->persist($commandeLigne);
            $total += $ligne->getProduit()->getPrix();
            $em->remove($ligne);
        }

        $em->persist($commande);
        $em->flush();

        $response = new Response("commande");
        $response->headers->set('Content-Type', 'application/text');
        $response->headers->set('Ok', 'oui');
        return $response;
    }

    /**
     * @Route("/apiPanierTotal/{id}",name="apiPanierTotal")
     */
    public function apiPanierTotal(EntityManagerInterface $em, $id) {
        $total = 0;
        $lesPanierLigne = $em->getRepository(PanierLigne::class)->findBy(['utilisateur' => $id]);

        foreach ($lesPanierLigne as $uneL) {
            $total += ($uneL->getProduit()->getPrix()) * $uneL->getQuantite();
        }
        $response = new Response($total);
        $response->headers->set('Content-Type', 'application/text');
        $response->headers->set('Ok', 'oui');
        return $response;
    }

    /**
     * @Route("/apiCommandes/{id}",name="apiCommandes")
     */
    public function apiCommandes(EntityManagerInterface $em, $id) {
        $lesCommandes = $em->getRepository(Commande::class)->findBy(['utilisateur' => $id]);
        $result = array();
        $i = 0;
        foreach ($lesCommandes as $uneCmd) {
            $al = array();
            $al['id'] = $uneCmd->getId();
            $al['status'] = $uneCmd->getStatus()->getLibelle();

            $total = 0;
            $lesCmdL = $em->getRepository(CommandeLigne::class)->findBy(['commande' => $uneCmd->getId()]);
            foreach ($lesCmdL as $uneCmdL) {
                $total += ($uneCmdL->getProduit()->getPrix()) * $uneCmdL->getQuantite();
            }
            $al['total'] = $total;
            $result[$i] = $al;
            $i++;
        }
        $serializer = $this->get('serializer');
        $data = $serializer->serialize($result, 'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Ok', 'oui');

        return $response;
    }

    /**
     * @Route("/apiProdCmd/{id}",name="apiProdCmd")
     */
    public function apiProdCmd(EntityManagerInterface $em, $id) {
        $lesCommandesL = $em->getRepository(CommandeLigne::class)->findBy(['commande' => $id]);
        $lesProd = array();
        $i = 0;
        foreach ($lesCommandesL as $uneCmdL) {
            $al = array();

            $unProduit = $em->getRepository(Produit::class)->find($uneCmdL->getProduit()->getId());
            $unProduit->setSelections(null);
            $unProduit->setCategorie(null);
            $al['produit'] = $unProduit;
            $al['quantite'] = $uneCmdL->getQuantite();

            $lesProd[$i] = $al;
            $i++;
        }

        $serializer = $this->get('serializer');
        $data = $serializer->serialize($lesProd, 'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Ok', 'oui');

        return $response;
    }

    /**
     * @Route("/apiQuantPanier",name="apiQuantPanier")
     */
    public function ajoutQuantPanier(EntityManagerInterface $em, Request $request) {
        $lnPanier = $em->getRepository(PanierLigne::class)->find($request->get("_id"));
        $quantite = $request->get("_quant");
        $quantPanier = $lnPanier->getQuantite();

        if ($quantite == 0) {
            $quantPanier = $quantPanier - 1;
        } else {
            $quantPanier = $quantPanier + 1;
        }
        if ($quantPanier > 0) {
            $lnPanier->setQuantite($quantPanier);
            $em->persist($lnPanier);
        } else {
            $em->remove($lnPanier);
        }
        $em->flush();

        $response = new Response("mis a jour");
        $response->headers->set('Content-Type', 'application/text');
        $response->headers->set('Ok', 'oui');
        return $response;
    }

}
