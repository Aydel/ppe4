<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Produit;
use App\Entity\PanierLigne;
use App\Entity\Commande;
use App\Entity\Adresse;
use App\Entity\Status;
use App\Entity\CommandeLigne;
use DateTime;
use App\Entity\Utilisateurs;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of panierController
 *
 * @author Aymerich
 */
class panierController extends AbstractController {

    /**
     * @Route("/panier", name="panier")
     */
    public function showPanier(EntityManagerInterface $em) {

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $listPanier = $em->getRepository(PanierLigne::class)->findBy(['utilisateur' => $user->getId()]);

        return $this->render('panier/panierList.html.twig', array(
                    'panier' => $listPanier,
        ));
    }

    /**
     * @Route("/buy",name="buyPanier")
     */
    public function buyPanier(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $utilisateur = $this->get('security.token_storage')->getToken()->getUser();

        $adressesUser = $em->getRepository(Adresse::class)->findBy(['utilisateur' => $utilisateur->getId()]);
        $produitsUser = $em->getRepository(PanierLigne::class)->findBy(['utilisateur' => $utilisateur->getId()]);

        if ($adressesUser != null) {

            $commande = new Commande();

            $commande->setUtilisateur($utilisateur);
            $commande->setAdresse($adressesUser[0]);

            $status = $em->getRepository(Status::class)->find(5);

            $commande->setStatus($status);
            $commande->setDate(new DateTime());

            $total = 0;

            foreach ($produitsUser as $ligne) {
                $commandeLigne = new CommandeLigne();
                $commandeLigne->setCommande($commande);
                $commandeLigne->setProduit($ligne->getProduit());
                $commandeLigne->setQuantite($ligne->getQuantite());
                $em->persist($commandeLigne);
                $total += $ligne->getProduit()->getPrix() * $ligne->getQuantite();
            }

            $em->persist($commande);
            $em->flush();

            return $this->render('commandes/checkout.html.twig', array(
                        'commande' => $commande,
                        'produits' => $produitsUser,
                        'total' => $total,
            ));
        } else {

            $adresse = new Adresse();
            $form = $this->createForm(\App\Form\AdresseType::class, $adresse);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                // ... perform some action, such as saving the task to the database
                // for example, if Task is a Doctrine entity, save it!
                $em = $this->getDoctrine()->getManager();
                $adresse->setUtilisateur($utilisateur);
                $em->persist($adresse);
                $em->flush();

                return $this->redirectToRoute('buyPanier');
            }

            return $this->render('commandes/adresse.html.twig', array(
                        'form' => $form->createView(),
            ));
        }



//        $userPanier = $em->getRepository(PanierLigne::class)->findBy(['utilisateur' => $user]);
        //$form = $this->createForm(\App\Form\CommandeType::class, $commande);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            // ... perform some action, such as saving the task to the database
//            // for example, if Task is a Doctrine entity, save it!
//            $em = $this->getDoctrine()->getManager();
//            
//            $em->persist($commande);
//            $em->flush();
//
//            return $this->redirectToRoute('accueil');
//        }
//
//        return $this->render('commande/create.html.twig', array(
//                    'form' => $form->createView(),
//        ));
    }

    /**
     * @Route("/confirm/{id}",name="confirm")
     */
    public function confirmBuy(EntityManagerInterface $em, $id) {
        $commande = $em->getRepository(Commande::class)->find($id);
        $status = $em->getRepository(Status::class)->find(1);

        if ($commande->getStatus() != $status) {
            $commande->setStatus($status);

            $produitsCommande = $em->getRepository(CommandeLigne::class)->findBy(['commande' => $id]);

            $utilisateur = $this->get('security.token_storage')->getToken()->getUser();
            $produitsUser = $em->getRepository(PanierLigne::class)->findBy(['utilisateur' => $utilisateur->getId()]);
            foreach ($produitsUser as $prod) {
                $em->remove($prod);
            }
            $em->flush();

            foreach ($produitsCommande as $ln) {
                $prodLn = $ln->getProduit();
                $stockLn = $prodLn->getStock();

                $newStock = $stockLn - $ln->getQuantite();
                $prodLn->setStock($newStock);

                $em->persist($prodLn);
                $em->flush();
            }


            $em->persist($commande);
            $em->flush();

            return $this->render('commandes/recap.html.twig', array(
                        'commande' => $commande
            ));
        } else {
            return $this->render('commandes/recap.html.twig', array(
                        'commande' => $commande
            ));
        }
    }

    /**
     * @Route("/pdf/{id}",name="pdf")
     */
    public function toPdfAction(EntityManagerInterface $em, $id) {
        $commandesLn = $em->getRepository(CommandeLigne::class)->findBy(['commande' => $id]);
        $commande = $em->getRepository(Commande::class)->find($id);

        $pdf = new \FPDF();
        $pdf->AddPage();
        $pdf->SetTitle('Commande N°' . $id);
        $pdf->SetFont('Arial', 'B', 16);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Image("Images/sylvie.jpg", 30, null, 50, 50);
        $pdf->Text(130, 40, "SDshop");
        $pdf->SetFont('Courier', 'B', 13);
        $pdf->Text(112, 50, "Client : " . $commande->getUtilisateur()->getNom() . ' ' . $commande->getUtilisateur()->getPrenom());

        $x = 65;
        $y = 100;
        $xi = 20;
        $yi = 85;
        $total = 0;
        foreach ($commandesLn as $ln) {
            $total+=$ln->getProduit()->getPrix();
            $pdf->Image("Images".$ln->getProduit()->getImage().".jpg", $xi, $yi, 40, 40);
            $pdf->Text($x, $y, $ln->getProduit()->getLibelle());
            $pdf->Text($x+50, $y, $ln->getProduit()->getPrix()." Euros TTC");
            $pdf->Text($x+90, $y, "Quantite : ".$ln->getQuantite());
            $y += 50;
            $yi += 50;
        }
        $pdf->Text($x, $y,"Total : ".$total." Euros TTC");
        return new Response($pdf->Output(), 200, array(
            'Content-Type' => 'application/pdf'
        ));
    }

    /**
     * @Route("/lcom",name="lcom")
     */
    public function listCommande(EntityManagerInterface $em) {
        $utilisateur = $this->get('security.token_storage')->getToken()->getUser();

        $commandes = $em->getRepository(Commande::class)->findBy([
            'utilisateur' => $utilisateur->getId(),
            'status' => 1,
        ]);

        $result = array();
        $i = 0;
        foreach ($commandes as $uneCommande) {
            $commandesLn = $em->getRepository(CommandeLigne::class)->findBy(['commande' => $uneCommande->getId()]);
            $status = $em->getRepository(Status::class)->find($uneCommande->getStatus());

            $y = 0;
            $total = 0;
            
            $result[$i]['Commande'] = $uneCommande;
            $result[$i]['status'] = $status->getLibelle();
            foreach ($commandesLn as $cmdLn) {
                $result[$i]['CommandesLignes'][$y] = $cmdLn;
                $y++;
                
                $total += $cmdLn->getProduit()->getPrix() * $cmdLn->getQuantite();
            }
            
            $result[$i]['total'] = $total;
            $i++;
        }
        //dump($result);die;
        return $this->render('commandes/list.html.twig', array(
                    'result' => $result,
        ));
    }

    /**
     * @Route("/retirerPanier/{id}",name="retirerPanier")
     */
    public function retirerPanier(EntityManagerInterface $em, $id) {
        $utilisateur = $this->get('security.token_storage')->getToken()->getUser();
        $prod = $em->getRepository(PanierLigne::class)->findBy(['utilisateur' => $utilisateur->getId(), 'produit' => $id]);
        $em->remove($prod[0]);
        $em->flush();

        $listPanier = $em->getRepository(PanierLigne::class)->findBy(['utilisateur' => $utilisateur->getId()]);

        return $this->render('panier/panierList.html.twig', array(
                    'panier' => $listPanier,
        ));
    }

    /**
     * @Route("/addQuantitePanier/{id}", name="addQuantitePanier")
     */
    public function addQuantitePanier(EntityManagerInterface $em, $id) {
        $ln = $em->getRepository(PanierLigne::class)->find($id);
        $quantite = $ln->getQuantite() + 1;
        $ln->setQuantite($quantite);
        $em->persist($ln);
        $em->flush();

        return $this->redirectToRoute("panier");
    }

    /**
     * @Route("/remQuantitePanier/{id}", name="remQuantitePanier")
     */
    public function remQuantitePanier(EntityManagerInterface $em, $id) {
        $ln = $em->getRepository(PanierLigne::class)->find($id);
        $quantite = $ln->getQuantite() - 1;
        if ($quantite == 0) {
            $em->remove($ln);
        } else {
            $ln->setQuantite($quantite);
            $em->persist($ln);
        }

        $em->flush();

        return $this->redirectToRoute("panier");
    }

}
