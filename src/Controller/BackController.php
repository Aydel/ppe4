<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Utilisateurs;
use App\Entity\Produit;
use App\Entity\Commande;
use App\Entity\CommandeLigne;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Description of BackController
 *
 * @author Aymerich
 */
class BackController extends AbstractController {

    /**
     * @Route("/administration",name="administration")
     */
    public function getInfos(EntityManagerInterface $em) {
        $users = $em->getRepository(Utilisateurs::class)->findAll();
        $nbUsers = sizeof($users);

        $produits = $em->getRepository(Produit::class)->findAll();
        $nbProduits = sizeof($produits);

        $commandesValides = $em->getRepository(Commande::class)->findBy(['status' => 1]);
        $nbCommandes = sizeof($commandesValides);

        $CA = 0;

        foreach ($commandesValides as $uneCmd) {
            $lesCommandesLn = $em->getRepository(CommandeLigne::class)->findBy(['commande' => $uneCmd->getId()]);

            foreach ($lesCommandesLn as $cmdLn) {
                $total = 0;
                $produit = $cmdLn->getProduit();
                $total = $produit->getPrix() * $cmdLn->getQuantite();
                $CA += $total;
            }
        }


        return $this->render('admin/admin.html.twig', array(
                    'nbUsers' => $nbUsers,
                    'nbProduits' => $nbProduits,
                    'nbCommandes' => $nbCommandes,
                    'CA' => $CA
        ));
    }

}
