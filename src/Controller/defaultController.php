<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
/**
 * Description of defaultController
 *
 * @author Aymerich
 */
class defaultController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController {
    
    /**
     * @Route("/",name="accueil")
     */
    public function accueil(EntityManagerInterface $em){
        
        return $this->render("default/defaultAccueil.html.twig");
    }
}
