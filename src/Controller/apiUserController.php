<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Utilisateurs;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of apiUserController
 *
 * @author Aymerich
 */
class apiUserController extends Controller {

    /**
     * @Route("/connexionApi",name="connexionApi")
     */
    public function connexionApi(Request $request, EntityManagerInterface $em) {
        
        $user = $em->getRepository(Utilisateurs::class)->findBy([
            "login" => $request->get('_username')]);

        $encoder = $this->get('security.password_encoder');

        // No salt in bcrypt, only iterations

        $validPassword = $encoder->isPasswordValid($user[0], $request->get('_password'));

        //https://symfony.com/doc/current/components/security/authentication.html

        if ($validPassword) {
            $response = new Response("Connected");
            $response->headers->set('Content-Type', 'application/text');
            $response->headers->set('Ok', 'oui');
            return $response;
        } else {
            $response = new Response("Erreur: Identifiants incorrects");
            $response->headers->set('Content-Type', 'application/text');
            $response->headers->set('Ok', 'non');
            return $response;
        }
    }

    /**
     * @Route("/userApi/{id}",name="userApi")
     */
    public function userApi(EntityManagerInterface $em, $id) {

        $userInfos = $em->getRepository(Utilisateurs::class)->findBy(['login' => $id]);
        //dump($userInfos);die;
        if ($userInfos[0] != null) {
            $userInfos[0]->setAdresses(null);
            $userInfos[0]->setCommandes(null);
            $userInfos[0]->setProduitsPanier(null);

            $serializer = $this->get('serializer');
            $data = $serializer->serialize($userInfos[0], 'json');


            $response = new Response($data);
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Ok', 'oui');
        } else {
            $response = new Response("ERREUR");
            $response->headers->set('Content-Type', 'application/text');
            $response->headers->set('Ok', 'non');
        }

        return $response;
    }

}
