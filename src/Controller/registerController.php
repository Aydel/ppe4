<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Entity\Utilisateurs;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;



/**
 * Description of registerController
 *
 * @author Aymerich
 */
class registerController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController {

    /**
     * @Route("/register",name="inscription")
     */
    public function inscription(Request $request) {
        $user = new Utilisateurs();
        $form = $this->createForm(\App\Form\UtilisateursType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $em = $this->getDoctrine()->getManager();
            $user->setRole("ROLE_USER");
            $user->setIsActive(1);
            $em->persist($user);
            $em->flush();

//            $test = $em->getRepository(Utilisateurs::class)->find($user);
//            $login = ($test->getId() + rand(100000000, 999999999));
//            $test->setLogin($login);
//            $em->persist($user);
//            $em->flush();

            return $this->redirectToRoute('login');
        }

        return $this->render('register/register.html.twig', array(
                    'form' => $form->createView(),
        ));
    }
    

}
