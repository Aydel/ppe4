<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\OptionsResolver\OptionsResolver;
/**
 * Description of AdresseType
 *
 * @author Aymerich
 */
class AdresseType extends AbstractType {

    public function buildform(FormBuilderInterface $builder, array $options){
        $builder->add('cp', TextType::class)
                ->add('rue', TextType::class)
                ->add('ville', TextType::class)
                ->add('save', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, array('label' => 'Valider'));
    }
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => \App\Entity\Adresse::class,
        ]);
    }
}

