<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Categorie
 * 
 * @ORM\Table(name="categorie")
 * @ORM\Entity
 */
class Categorie {
    
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * 
     */
    private $id;
    
    /**
     *
     * @var string
     * 
     * @ORM\Column(name="intitule", type="string", nullable=true) 
     */
    private $intitule;
    
    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Produit", mappedBy="categorie") 
     * 
     */
    private $lesProduits;
    
    function getId() {
        return $this->id;
    }

    function getIntitule() {
        return $this->intitule;
    }

    function getLesProduits() {
        return $this->lesProduits;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setIntitule($intitule) {
        $this->intitule = $intitule;
    }

    function setLesProduits($lesProduits) {
        $this->lesProduits = $lesProduits;
    }


}
