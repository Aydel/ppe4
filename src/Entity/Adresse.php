<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * panier
 *
 * @ORM\Table(name="adresse")
 * @ORM\Entity
 */
class Adresse {
    
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * 
     */
    private $id;
    
    /**
     *
     * @var int 
     * 
     * @ORM\Column(name="cp", type="integer", nullable=false)
     */
    private $cp;
    
    /**
     *
     * @var string
     * 
     * @ORM\Column(name="rue", type="string", nullable=false) 
     */
    private $rue;
    
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="ville", type="string", nullable=false)
     */
    private $ville;
    
    /**
     * @var int
     * 
     * @ManyToOne(targetEntity="Utilisateurs",inversedBy="adresses")
     * @JoinColumn(nullable=true)
     * 
     */
    private $utilisateur;
    
    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Commande", mappedBy="adresse") 
     * 
     */
    private $commandes;
    
    function __construct() {
        $this->commandes = new ArrayCollection();
    }

    function getId() {
        return $this->id;
    }

    function getCp() {
        return $this->cp;
    }

    function getRue() {
        return $this->rue;
    }

    function getVille() {
        return $this->ville;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCp($cp) {
        $this->cp = $cp;
    }

    function setRue($rue) {
        $this->rue = $rue;
    }

    function setVille($ville) {
        $this->ville = $ville;
    }

    function getUtilisateur() {
        return $this->utilisateur;
    }

    function setUtilisateur($utilisateur) {
        $this->utilisateur = $utilisateur;
    }

    function getCommandes() {
        return $this->commandes;
    }

    function setCommandes($commandes) {
        $this->commandes = $commandes;
    }
    public function __toString() {
        return (string) $this->id;
    }
    



}
