<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * commande
 *
 * @ORM\Table(name="commande")
 * @ORM\Entity
 */
class Commande {
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    
    /**
     * @var int
     * 
     * @ManyToOne(targetEntity="Utilisateurs",inversedBy="commandes")
     * @JoinColumn(nullable=true)
     * 
     */
    private $utilisateur;
    
    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\PanierLigne", mappedBy="utilisateur") 
     * 
     */
    private $panier;
    
    /**
     *
     * @var datetime
     * 
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;
    
    /**
     *
     * @ManyToOne(targetEntity="Status",inversedBy="commandes")
     * @JoinColumn(nullable=true)
     */
    private $status;
    
    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\CommandeLigne", mappedBy="commande") 
     * 
     */
    private $produitsCommande;
    
    /**
     * @var int
     * 
     * @ManyToOne(targetEntity="Adresse",inversedBy="commandes")
     * @JoinColumn(nullable=true)
     * 
     */
    private $adresse;
    
    public function __construct() {
        $this->panier = new ArrayCollection();
        $this->produitsCommande = new ArrayCollection();
        
    }
    
    function getId() {
        return $this->id;
    }

    function getUtilisateur() {
        return $this->utilisateur;
    }

    function getPanier() {
        return $this->panier;
    }

    function getDate(){
        return $this->date;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function getStatus() {
        return $this->status;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUtilisateur($utilisateur) {
        $this->utilisateur = $utilisateur;
    }

    function setPanier($panier) {
        $this->panier = $panier;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    function setStatus($status) {
        $this->status = $status;
    }
    public function __toString() {
        return (string) $this->id;
    }

    
    
}
