<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Serializable;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Utilisateurs
 *
 * @ORM\Table(name="utilisateurs")
 * @ORM\Entity
 */
class Utilisateurs implements UserInterface, Serializable {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=false)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=255, nullable=true)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @ORM\Column(type="string",length=255)
     */
    private $role;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\PanierLigne", mappedBy="utilisateur") 
     * 
     */
    private $produitsPanier;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Commande", mappedBy="utilisateur") 
     * 
     */
    private $commandes;
    
    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Adresse", mappedBy="utilisateur") 
     * 
     */
    private $adresses;

    public function __construct() {
        $this->produitsPanier = new ArrayCollection();
        $this->commandes = new ArrayCollection();
    }

    function getId() {
        return $this->id;
    }

    function getNom() {
        return $this->nom;
    }

    function getPrenom() {
        return $this->prenom;
    }

    function getLogin() {
        return $this->login;
    }

    function getPassword() {
        return $this->password;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setPrenom($prenom) {
        $this->prenom = $prenom;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function getRole() {
        return $this->role;
    }

    function setRole($role) {
        $this->role = $role;
    }

    function getIsActive() {
        return $this->isActive;
    }

    function setIsActive($isActive) {
        $this->isActive = $isActive;
    }

    public function setPassword($password) {
        $this->password = password_hash($password, PASSWORD_BCRYPT, array("cost" => 13));
    }

    public function __toString() {
        return (string) $this->id;
    }

    public function eraseCredentials() {
        
    }

    public function getRoles() {
        return array($this->role);
    }

    public function getSalt() {
        return null;
    }

    public function getUsername() {
        return $this->getLogin();
    }

    public function serialize() {
        return serialize(array(
            $this->id,
            $this->login,
            $this->password
        ));
    }

    public function unserialize($serialized) {
        list (
                $this->id,
                $this->login,
                $this->password
                ) = unserialize($serialized);
    }

    function getProduitsPanier() {
        return $this->produitsPanier;
    }

    function setProduitsPanier($produitsPanier) {
        $this->produitsPanier = $produitsPanier;
    }
    
    function getCommandes() {
        return $this->commandes;
    }

    function getAdresses() {
        return $this->adresses;
    }

    function setCommandes($commandes) {
        $this->commandes = $commandes;
    }

    function setAdresses($adresses) {
        $this->adresses = $adresses;
    }



}
