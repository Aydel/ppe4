<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * panier
 *
 * @ORM\Table(name="panierLigne")
 * @ORM\Entity
 */
class PanierLigne {

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * 
     */
    private $id;

    /**
     * @var int
     * 
     * @ManyToOne(targetEntity="Utilisateurs",inversedBy="produitsPanier")
     * @JoinColumn(nullable=true)
     * 
     */
    private $utilisateur;
    
    /**
     * @var int
     *
     * @ManyToOne(targetEntity="Produit",inversedBy="selections")
     * @JoinColumn(nullable=false)
     */
    private $produit;
    
    /**
     * @var int
     * 
     * @ORM\Column(name="quantite", type="integer", nullable=false)
     */
    private $quantite;
    
    function getId() {
        return $this->id;
    }

    function getProduit() {
        return $this->produit;
    }

    function getQuantite() {
        return $this->quantite;
    }

    function setProduit($produit) {
        $this->produit = $produit;
    }

    function setQuantite($quantite) {
        $this->quantite = $quantite;
    }
    
    function getUtilisateur() {
        return $this->utilisateur;
    }

    function setUtilisateur($utilisateur) {
        $this->utilisateur = $utilisateur;
    }
    
    public function __toString() {
        return (string) $this->id;
    }



}
