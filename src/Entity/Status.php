<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * panier
 *
 * @ORM\Table(name="status")
 * @ORM\Entity
 */
class Status {
    
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * 
     */
    private $id;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="libelle", type="string", nullable=false)
     */
    private $libelle;
    
    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Commande", mappedBy="status") 
     * 
     */
    private $commandes;
    
    function getId() {
        return $this->id;
    }

    function getLibelle() {
        return $this->libelle;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setLibelle($libelle) {
        $this->libelle = $libelle;
    }
    
    function getCommandes() {
        return $this->commandes;
    }

    function setCommandes($commandes) {
        $this->commandes = $commandes;
    }

    public function __toString() {
        return (string) $this->id;
    }



}
