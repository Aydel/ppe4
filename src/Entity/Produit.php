<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * Produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity
 */
class Produit {
    
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255, nullable=false)
     */
    private $libelle;
    
    /**
     *
     * @var string
     * 
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;
    
    /**
     *
     * @var int
     * 
     * @ORM\Column(name="prix", type="integer", nullable=false)
     */
    private $prix;
    
    /**
     *
     * @var string
     * 
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;
    
    /**
     *
     * @var int
     * 
     * @ORM\Column(name="stock", type="integer", nullable=false)
     */
    private $stock;
    
    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\PanierLigne", mappedBy="produit") 
     * 
     */
    private $selections;
    
    /**
     *
     * @ManyToOne(targetEntity="Categorie",inversedBy="lesProduits")
     * @JoinColumn(nullable=true)
     */
    private $categorie;
    
    function __construct() {
        $this->selections = new ArrayCollection();
    }

    function getId() {
        return $this->id;
    }

    function getLibelle() {
        return $this->libelle;
    }

    function getDescription() {
        return $this->description;
    }

    function getPrix() {
        return $this->prix;
    }

    function getImage() {
        return $this->image;
    }

    function getStock() {
        return $this->stock;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setPrix($prix) {
        $this->prix = $prix;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setStock($stock) {
        $this->stock = $stock;
    }

    function getSelections() {
        return $this->selections;
    }

    function setSelections($selections) {
        $this->selections = $selections;
    }

    function getCategorie() {
        return $this->categorie;
    }

    function setCategorie($categorie) {
        $this->categorie = $categorie;
    }



}
